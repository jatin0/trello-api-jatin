var key = 'b59de0490c40dae1c0409907607fc85d';
var token = '1c9dce0dc0b1fe3eb2decbbace085c9235b08b636b72b12bef23f8670246dba6';
var listId = '5d09ce4e25e47417de02b95d';

function getListOfCards() {
	return fetch(`https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`).then((val) => val.json());
}

function getChecklists(cardId) {
	return fetch(`https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}`).then((res) =>
		res.json()
	);
}

function getChecklistOfCards() {
	return getListOfCards()
		.then((cards) => Promise.all(cards.map((card) => getChecklists(card.id))))
		.then((data) => data.flat())
		.then((val) => {
			val.map((check) => checkItemsOnPage(check.checkItems, check.idCard));
		});
}

function checkItemsOnPage(item, idcard) {
	item.map((data) => {
		let state = '';
		if (data.state === 'complete') {
			state = 'checked';
		}
		$('#check-list').append(
			`<li id="btn" data-cardid="${idcard}" data-checklistItemId="${data.id}" data-checklistId="${data.idChecklist}">
					<input class="checkbtn" type="checkbox" ${state} > <span id="name" class="${data.state}">${data.name}</span>
					<input id="deletebtn" type = 'button' value='x'></li>`
		);
	});
}
getChecklistOfCards();

function checkupdate() {
	$('#check-list').on('click', 'input[type="checkbox"]', function(e) {
		// e.preventDefault();
		let cid = $(this).parent().attr('data-cardid');
		let item = $(this).parent().attr('data-checklistItemId');
		let valcheck = this.checked ? 'complete' : 'incomplete';

		if ($('.checkbtn').is(':checked')) {
			$(this).parent().contents('#name').removeClass('incomplete').addClass('complete');
			fetch(
				`https://api.trello.com/1/cards/${cid}/checkItem/${item}?state=${valcheck}&key=${key}&token=${token}`,
				{ method: 'PUT' }
			);
		} else {
			$(this).parent().contents('#name').removeClass('complete').addClass('incomplete');
			fetch(
				`https://api.trello.com/1/cards/${cid}/checkItem/${item}?state=${valcheck}&key=${key}&token=${token}`,
				{ method: 'PUT' }
			);
		}
		if ($(this).prop('checked')) {
			$(this).siblings('span').removeClass('incomplete');
			$(this).siblings('span').addClass('complete');
		} else {
			$(this).siblings('span').removeClass('complete');
			$(this).siblings('span').addClass('incomplete');
		}
	});
}

checkupdate();

function deleteItem() {
	$('#check-list').on('click', '#deletebtn', function(event) {
		let itemid = $(this).parent().attr('data-checklistItemId');
		let checkid = $(this).parent().attr('data-checklistId');
		let res = fetch(
			`https://api.trello.com/1/checklists/${checkid}/checkItems/${itemid}?&key=${key}&token=${token}`,
			{ method: 'DELETE' }
		);
		res.then((item) => {
			if (item.status === 200) {
				$(this).parent().remove();
			}
		});
	});
}

deleteItem();

function addItems() {
	// let cardid = '5d0a852ce7a4290580ebf462';
	let checklistid = '5d0a8564a4701132c64bd1d0';
	$('#addform').on('submit', async function(event) {
		let cardid = $(this).siblings('li').attr('data-cardid');
		event.preventDefault();
		let itemName = $(this).children('#add').val();
		console.log(itemName);
		let res = await fetch(
			`https://api.trello.com/1/checklists/${checklistid}/checkItems?name=${itemName}&key=${key}&token=${token}`,
			{ method: 'POST' }
		).then((item) => item.json());
		let state = '';
		if (res.state === 'complete') {
			state = 'checked';
		}
		$('#check-list').append(
			`<li id="btn" data-cardid="${cardid}" data-checklistItemId="${res.id}" data-checklistId="${res.idChecklist}">
                        <input class="checkbtn" type="checkbox" ${state}> <span id="name" class="${res.state}" data-cardid="${cardid}">${itemName}</span>
                        <input id="deletebtn" type = 'button' value='x'></li>`
		);  
		$(this).children('#add').val('');
	});
}

addItems();
