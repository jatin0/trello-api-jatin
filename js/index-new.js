var key = 'b59de0490c40dae1c0409907607fc85d';
var token = '1c9dce0dc0b1fe3eb2decbbace085c9235b08b636b72b12bef23f8670246dba6';
var boardId = 'NzPMLNDZ';

function getListOfCards() {
	return fetch(
		`https://api.trello.com/1/lists/5d09ce4e25e47417de02b95d/cards?key=${key}&token=${token}`
	).then((val) => val.json());
}

function getChecklists(cardId) {
	return fetch(`https://api.trello.com/1/cards/${cardId}/checklists?key=${key}&token=${token}`).then((res) =>
		res.json()
	);
}

function getChecklistOfCards() {
	return getListOfCards()
		.then((cards) => Promise.all(cards.map((card) => getChecklists(card.id))))
		.then((data) => data.flat())
		.then((val) => {
			val.map((check) => checkItemsOnPage(check.checkItems, check.idCard));
		});
}

async function checkItemsOnPage(item, idcard) {
	item.map((data) => {
		let checked = '';
		if (data.state === 'complete') {
			checked = 'checked';
		}
		else {
			checked = 'unchecked' ;
		}
		$('#check-list').append(
			`<li id="btn" cardid="${idcard}" checklistItemId="${data.id}" checklistId="${data.idChecklist}">
					<input class="checkbtn" type="checkbox" ${checked} > <span id="name" class="${data.state}">${data.name}</span>
					<input id="deletebtn" type = 'button' value='x'></li>`
		);
		console.log(data.idChecklist);
	});
	$('.checkbtn').change(function() {
		let cid = $(this).parent().attr('cardid');
		let item = $(this).parent().attr('checklistItemId');
		let valcheck = this.checked ? 'complete' : 'incomplete';
		if ($(this).siblings('#name').attr('class') === 'complete') {
			$(this).parent().contents('#name').removeClass('complete').addClass('incomplete');
			fetch(
				`https://api.trello.com/1/cards/${cid}/checkItem/${item}?state=${valcheck}&key=${key}&token=${token}`,
				{ method: 'PUT' }
			);
		} else {
			$(this).parent().contents('#name').removeClass('incomplete').addClass('complete');
			fetch(
				`https://api.trello.com/1/cards/${cid}/checkItem/${item}?state=${valcheck}&key=${key}&token=${token}`,
				{ method: 'PUT' }
			);
		}
	});
}

getChecklistOfCards();

async function addItems() {
		$('#addform').on('submit', function(event) {
		event.preventDefault();
		let cardid = '5d0a8564a4701132c64bd1d0';
		let itemName = $(this).children('#add').val();
		let res = fetch(
			`https://api.trello.com/1/checklists/${cardid}/checkItems?name=${itemName}&key=${key}&token=${token}`,
			{
				method: 'POST'
			}
		).then(item => item.json());

		// res.then((data) => {
		// 	if (data.status === 200) {
				$('#check-list').append(
					`<li id="btn" cardid="${cardid}" checklistItemId="${res.id}" checklistId="${res.idChecklist}">
								<input class="checkbtn" type="checkbox" > <span class="${res.state}">${itemName}</span>
								<input id="deletebtn" type = 'button' value='x'></li>`
				);
			// }
			// console.log(data.id)
			// $(this).children('#add').val('');
		// });
	});
}

addItems();

async function deleteItem() {
	await $('#check-list').on('click', '#deletebtn', function(event) {
		let itemid = $(this).parent().attr('checklistItemId');
		let checkid = $(this).parent().attr('checklistId');
		console.log(itemid);
		console.log(checkid);

		let res = fetch(
			`https://api.trello.com/1/checklists/${checkid}/checkItems/${itemid}?&key=${key}&token=${token}`,
			{ method: 'DELETE' }
		);
		res.then((item) => {
			if (item.status === 200) {
				$(this).parent().remove();
			}
		});
	});
}

deleteItem();

// -------------------------------------------------------------------------------------

// async function getChecklistOfCards() {
// 	return getListOfCards()
// 		.then((cards) => Promise.all(cards.map((card) => getChecklists(card.id))))
// 		.then((data) => data.flat())
// 		.then(data=>console.log(data))
// 		.then((val) => val.map((check) => check.checkItems));
// }

// function checkItemsOnPage() {
// 	getChecklistOfCards().then((item) => {
// 		item.map((val) => {
// 			val.map((data) => {
// 				$('#check-list').append(
// 					`<p class="btn" checklistItemId=${data.id} checklistState=${data.state} checklistId=${data.idChecklist}>
// 					<input type="checkbox">${data.name}
// 					<input id="deletebtn" type = 'button' value='x'></p>`
// 				);
// 			});
// 		});
// 	});
// }
